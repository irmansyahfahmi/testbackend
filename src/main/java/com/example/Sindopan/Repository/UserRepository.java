package com.example.Sindopan.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Sindopan.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Integer>{

}
